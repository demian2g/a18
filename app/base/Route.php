<?php

namespace app\base;

/**
 * Class Route
 * @package base
 */

class Route {

    const DEFAULT_CONTROLLER = 'site';
    const DEFAULT_ACTION = 'index';

    const CONTROLLER_NAMESPACE = '\app\controllers\\';

    public $action;
    public $controller;
    public $_queryParams = null;

    private function init_routes() {
        $routes = explode('/', parse_url($_SERVER['REQUEST_URI'])['path']);

        $this->controller = (!empty($routes[1])) ? strtolower($routes[1]) : self::DEFAULT_CONTROLLER;
        $this->action = (!empty($routes[2])) ? strtolower($routes[2]) : self::DEFAULT_ACTION;
        if (count($routes) == 2 && !empty($routes[1])){
            $this->controller = self::DEFAULT_CONTROLLER;
            $this->action = strtolower($routes[1]);
        }

        if (isset(parse_url($_SERVER['REQUEST_URI'])['query'])){
            parse_str(parse_url($_SERVER['REQUEST_URI'])['query'], $this->_queryParams);
        }
    }

    public function init() {

        $this->init_routes();

        $controller = ucfirst($this->controller) . 'Controller';
        $action = 'action' . ucfirst($this->action);

        $controllerNS = self::CONTROLLER_NAMESPACE . $controller;
        $this->run(new $controllerNS, $action, $this->_queryParams);

    }

    /**
     * TODO: Обработчик ошибок вызова экшна
     * @param $controller
     * @param $action
     * @param $params
     */
    private function run($controller, $action, $params){
        if (!method_exists($controller, $action)){
            header("HTTP/1.0 404 Not Found");
            echo "404 Not found.\n";
            die();
        } else
            $controller->$action($params);
    }
}