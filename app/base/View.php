<?php

namespace app\base;
use MatthiasMullie\Minify\CSS;
use MatthiasMullie\Minify\JS;

/**
 * Class View
 * @package app\base
 */

class View {

    public $layout = 'main';
    public $modelName;
    private $_assetsGlobal;
    private $_assetsCurrent;

    public function __construct() {
        $this->_assetsGlobal = include_once __DIR__ . '/../../config/assets.php';
        if (isset($this->_assetsGlobal[$this->layout])){
            $this->_assetsCurrent = $this->_assetsGlobal[$this->layout];
        }
    }

    public function render($__view, $data = []){
        extract($data);
        return include 'app/layouts/' . $this->layout . '.php';
    }

    public function getCss($dev_mode = false){
        if ($dev_mode) {
            if (FORCE_COPY) {
                $css = null;
                foreach ($this->_assetsCurrent['css'] as $item) {
                    $items = explode('/', $item);
                    $file = file_get_contents($item);
                    $fileName = end($items);
                    file_put_contents(__DIR__ . '/../../css/' . $fileName, $file);
                    $css .= "\n" . '<link href = "/css/' . $fileName . '" rel="stylesheet">';
                }
                return $css;
            } else {
                $css = null;
                foreach ($this->_assetsCurrent['css'] as $item) {
                    $items = explode('/', $item);
                    $fileName = end($items);
                    $css .= "\n" . '<link href = "/css/' . $fileName . '" rel="stylesheet">';
                }
                return $css;
            }
        } else {
            if (FORCE_COPY) {
                $cssMinifier = new CSS();
                foreach ($this->_assetsCurrent['css'] as $item) {
                    $cssMinifier->add($item);
                }
                $cssMinifier->minify(__DIR__ . '/../../css/app.css');
                return '<link href="/css/app.css" rel="stylesheet">' . "\n";
            } else {
                return '<link href="/css/app.css" rel="stylesheet">' . "\n";
            }
        }
    }

    public function getJs($dev_mode = false){
        if ($dev_mode) {
            if (FORCE_COPY) {
                $js = null;
                foreach ($this->_assetsCurrent['js'] as $item) {
                    $items = explode('/', $item);
                    $file = file_get_contents($item);
                    $fileName = end($items);
                    file_put_contents(__DIR__ . '/../../js/' . $fileName, $file);
                    $js .= "\n" . '<script src = "/js/' . $fileName . '"></script>';
                }
                return $js;
            } else {
                $js = null;
                foreach ($this->_assetsCurrent['js'] as $item) {
                    $items = explode('/', $item);
                    $fileName = end($items);
                    $js .= "\n" . '<script src = "/js/' . $fileName . '"></script>';
                }
                return $js;
            }
        } else {
            if (FORCE_COPY) {
                $jsMinifier = new JS();
                foreach ($this->_assetsCurrent['js'] as $item) {
                    $jsMinifier->add($item);
                }
                $jsMinifier->minify(__DIR__ . '/../../js/app.js');
                return '<script src="/js/app.js"></script>' . "\n";
            } else {
                return '<script src="/js/app.js"></script>' . "\n";
            }
        }
    }
}