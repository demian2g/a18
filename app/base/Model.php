<?php

namespace app\base;

/**
 * Class Model
 * @package app\base
 */

class Model extends Query {

    /**
     * @param array $attributesArray array
     */
    public function __construct($attributesArray = []){
        foreach ($attributesArray as $attribute => $value) {
            if (array_key_exists($attribute, get_object_vars($this))) {
                $this->$attribute = $value;
            }
        }
        parent::__construct();
    }

}