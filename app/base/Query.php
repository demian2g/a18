<?php

namespace app\base;

class Query extends DB {

    private $db;
    private $searchQuery;

    public function __construct() {
        $this->db = DB::getInstance();
    }

    public static function tableName(){ }

    public static function classNameStatic(){
        return lcfirst(get_called_class());
    }

    public function save(){
        $query = null;
        if (in_array('id', $this->schema)) unset($this->schema[0]);
        if (empty($this->id)) {
            $query = 'INSERT INTO ' . $this::tableName() . ' (' . $this->getSchema() . ') VALUES (' . $this->getINSERTValues() . ')';
        } else {
            $query = 'UPDATE ' . $this::tableName() . ' SET ' . $this->getUPDATEValues() . ' WHERE id=' . (int)$this->id;
        }
        try {
            $this->db->exec($query);
        } catch (\PDOException $e) {
            echo $e->getMessage() ; exit;
        }
    }

    public function find(){
        $this->searchQuery['select'] = '*';
        return $this;
    }

    public function select($columns = '*'){
        if (is_string($columns))
            $this->searchQuery['select'] = $columns;
        if (is_array($columns))
            $this->searchQuery['select'] = implode(', ', array_map(function ($item){return '`' . $item .'`';}, $columns));
        return $this;
    }

    public function where($columns){
        $this->searchQuery['where'] = null;
        if (is_string($columns))
            $this->searchQuery['where'] = $columns;
        if (is_array($columns)) {
            $values = [];
            foreach ($columns as $column => $value) {
                if (is_int($value)) $values[] = "`$column` = " . (int)$value;
                elseif ($value === NULL) $values[] = "`$column` = NULL";
                else $values[] = "`$column` = " . "'" . $value . "'";
            }
            $this->searchQuery['where'] = implode(' AND ', $values);
        }
        return $this;
    }

    public function join(array $join){
        $this->searchQuery['join'] = [];
        if (!empty($join))
            $this->searchQuery['join'] = $join;
        return $this;
    }

    public function limit($limit){
        $this->searchQuery['limit'] = (int)$limit;
        return $this;
    }

    public function offset($offset){
        $this->searchQuery['offset'] = (int)$offset;
        return $this;
    }

    public function one(){
        $query = $this->getSELECTQuery();
        try {
            return $this->db->query($query)->fetchObject($this::className());
        } catch (\PDOException $e) {
            echo $e->getMessage() ; exit;
        }
    }

    public function all(){
        $query = $this->getSELECTQuery();
        try {
            return $this->db->query($query)->fetchAll(\PDO::FETCH_CLASS, $this::className());
        } catch (\PDOException $e) {
            echo $e->getMessage() ; exit;
        }
    }

    public function raw(){
        $query = $this->getSELECTQuery();
        try {
            return $this->db->query($query)->fetchAll(\PDO::FETCH_ASSOC);
        } catch (\PDOException $e) {
            echo $e->getMessage() ; exit;
        }
    }

    protected function className(){
        return lcfirst(get_called_class());
    }

    private function getSchema(){
        return implode(', ', $this->schema);
    }

    private function getINSERTValues(){
        $values = [];
        foreach ($this->schema as $attribute) {
            if (is_int($this->$attribute)) $values[] = (int)$this->$attribute;
            elseif ($this->$attribute === NULL) $values[] = 'NULL';
            else $values[] = "'" . $this->$attribute . "'";
        }
        return implode(', ', $values);
    }

    private function getUPDATEValues(){
        $values = [];
        foreach ($this->schema as $attribute) {
            if (is_int($this->$attribute)) $values[] = "$attribute=" . (int)$this->$attribute;
            elseif ($this->$attribute === NULL) $values[] = "$attribute=NULL";
            else $values[] = "$attribute=" . "'" . $this->$attribute . "'";
        }
        return implode(', ', $values);
    }

    private function getSELECTQuery(){
        $query = null;
        if (empty($this->searchQuery['select']))
            throw new \Exception('No select query');
        $query = 'SELECT ' . $this->searchQuery['select'] . ' FROM `' . $this::tableName() .'`';
        if (!empty($this->searchQuery['join'])) {
            foreach ($this->searchQuery['join'] as $table => $expression) {
                $query .= ' JOIN `' . $table . '` ON ' . $expression;
            }
        }
        if (!empty($this->searchQuery['where']))
            $query .= ' WHERE ' . $this->searchQuery['where'];
        return $query;
    }
}