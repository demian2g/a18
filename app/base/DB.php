<?php

namespace app\base;

class DB {

    public $schema = [];

    private static $instance;

    /**
     * @return \PDO
     */
    public static function getInstance()
    {
        if( is_null(self::$instance) ) {
            self::$instance = self::connect();
        }
        return self::$instance;
    }

    private static function connect() {

        $dbConnection = new \PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS);
        $dbConnection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $dbConnection->exec("set names utf8");

        return $dbConnection;
    }
}