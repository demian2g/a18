<?php

include_once (file_exists('config/main.local.php') ? 'config/main.local.php' : 'config/main.php');
include_once __DIR__ . '/../vendor/autoload.php';

function autoload($className)
{
    $className = ltrim($className, '\\');
    $fileName  = '';
    $namespace = '';
    if ($lastNsPos = strrpos($className, '\\')) {
        $namespace = substr($className, 0, $lastNsPos);
        $className = substr($className, $lastNsPos + 1);
        $fileName  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
    }
    $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';

    if (file_exists($fileName))
        require $fileName;
    else {
        header("HTTP/1.0 404 Not Found");
        echo "404 Not found.\n";
        die();
    }
}
spl_autoload_register('autoload');

(new \app\base\Route())->init();