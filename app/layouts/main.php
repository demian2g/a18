<?php
/**
 * @var \app\base\View $this
 */
?>
<!DOCTYPE html>
<html lang="ru">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="TestTask">
    <meta name="author" content="demian2g">

    <title>TestTask MVC</title>
    <?= $this->getCss(APP_DEV) ?>

</head>
<body>
    <div class="y-g-responsive">
        <?php include 'app/views/' . $this->modelName . '/' . $__view . '.php'; ?>
    </div>
    <?= $this->getJs(APP_DEV) ?>
</body>
</html>