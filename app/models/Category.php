<?php

namespace app\models;

use app\base\Model;

class Category extends Model {

    /** @var integer $id */
    public $id;
    /** @var string $title */
    public $title;

    public $schema = ['id', 'title'];

    public static function tableName() {
        return 'category';
    }
}