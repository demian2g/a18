<?php

namespace app\models;

use app\base\Model;

class Result extends Model {

    /** @var integer $id */
    public $id;
    /** @var integer $player_id */
    public $player_id;
    /** @var string $game */
    public $game;
    /** @var integer $point_id */
    public $point_id;
    /** @var integer $point_position */
    public $point_position;
    /** @var string $time */
    public $time;

    public $schema = ['id', 'player_id', 'game', 'point_id', 'point_position', 'time'];

    public static function tableName() {
        return 'result';
    }

}