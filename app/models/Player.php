<?php

namespace app\models;

use app\base\Model;

class Player extends Model {

    /** @var integer $id */
    public $id;
    /** @var string $name */
    public $name;
    /** @var string $team */
    public $team;
    /** @var integer $category_id */
    public $category_id;
    /** @var string $number */
    public $number;
    /** @var string $chip */
    public $chip;
    /** @var string $comment */
    public $comment;

    public $schema = ['id', 'name', 'team', 'category_id', 'number', 'chip', 'comment'];

    public static function tableName() {
        return 'player';
    }

    public static function getAllPlayers(){
        return (new self())->find()->raw();
    }
}