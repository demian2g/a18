<?php

namespace app\controllers;

use app\base\Controller;

/**
 * Class SiteController
 * @package controllers
 */

class SiteController extends Controller {

    public function actionDay1(){
        return $this->view->render('day1', ['player' => $this->player]);
    }

}