<?php
namespace app\controllers;

use app\base\Controller;
use app\models\Category;
use app\models\Player;
use app\models\Result;

class ImporterController extends Controller {

    public function actionPlayers(){
        die('Already done;');

        $dir = __DIR__ . '/../../';
        $file = file_get_contents($dir . 'users.html');
        preg_match_all('/([абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ_]+)\s+[абвгдеёжзийклмнопрстуфхцчшщъыьэюяАБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ_|\s]+\s+(\d+)/', $file, $matches);
        unset($matches[1][11]);
        unset($matches[2][11]);

        $player = new Player();
        $player->category_id = 1;
        foreach (array_combine(array_slice($matches[2], 0, 53), array_slice($matches[1], 0, 53)) as $number => $name) {
            $clone = clone $player;
            $clone->name = $name;
            $clone->number = $number;
            $clone->save();
        }
        $player->category_id = 2;
        foreach (array_combine(array_slice($matches[2], 53, 58), array_slice($matches[1], 53, 58)) as $number => $name) {
            $clone = clone $player;
            $clone->name = $name;
            $clone->number = $number;
            $clone->save();
        }
        $player->category_id = 3;
        foreach (array_combine(array_slice($matches[2], 53+58, 11), array_slice($matches[1], 53+58, 11)) as $number => $name) {
            $clone = clone $player;
            $clone->name = $name;
            $clone->number = $number;
            $clone->save();
        }
        $player->category_id = 4;
        foreach (array_combine(array_slice($matches[2], 53+58+11, 16), array_slice($matches[1], 53+58+11, 16)) as $number => $name) {
            $clone = clone $player;
            $clone->name = $name;
            $clone->number = $number;
            $clone->save();
        }
        $player->category_id = 5;
        foreach (array_combine(array_slice($matches[2], 53+58+11+16, 18), array_slice($matches[1], 53+58+11+16, 18)) as $number => $name) {
            $clone = clone $player;
            $clone->name = $name;
            $clone->number = $number;
            $clone->save();
        }
        $player->category_id = 6;
        foreach (array_combine(array_slice($matches[2], 53+58+11+16+18, 11), array_slice($matches[1], 53+58+11+16+18, 11)) as $number => $name) {
            $clone = clone $player;
            $clone->name = $name;
            $clone->number = $number;
            $clone->save();
        }
    }

    public function actionDay1(){
        die('Already done;');

        $dir = __DIR__ . '/../../';
        $file = file_get_contents($dir . 'day1.html');
        $parts = explode('<h2>', $file);
        unset($parts[0]);
        foreach ($parts as $part) {
            preg_match_all('/(\d+)\s+\d{2}:\d{2}:\d{2}\s+\+/', $part, $matches);
            $numbers = $matches[1];
            unset($matches);
            preg_match_all('/(\d{2}:\d{2}:\d{2})\(\s*(\d{2,3})\)/', $part, $matches);
            $i = 0;
            foreach ($numbers as $number) {
                $a = array_slice($matches[2], $i * 20, 10);
                $b = array_slice($matches[1], $i * 20, 10);
                $point_id = 0;
                foreach (array_combine($a, $b) as $point => $time) {
                    $result = new Result(['player_id' => $number, 'game' => 'day1']);
                    $result->point_id = $point;
                    $result->point_position =$point_id;
                    $result->time = $time;
                    $result->save();
                    $point_id++;
                };
                $i++;
            }
        }
    }

    public function actionDay2(){
        die('Already done;');

        $dir = __DIR__ . '/../../';
        $file = file_get_contents($dir . 'day2.html');
        $parts = explode('<hr>', $file);
        unset($parts[0]);
        foreach ($parts as $part) {
            preg_match_all('/№\s(\d+)/', iconv('Windows-1251', 'UTF-8', $part), $matches);
            $numbers = $matches[1];
            unset($matches);
            preg_match_all('/\(\s*(\d{2,3})\)\s\d{2}:\d{2}:\d{2}\s(\d{2}:\d{2}:\d{2})/', $part, $matches);
            $i = 0;
            foreach ($numbers as $number) {
                $point_id = 0;
                foreach (array_combine($matches[1], $matches[2]) as $point => $time) {
                    $result = new Result(['player_id' => $number, 'game' => 'day2']);
                    $result->point_id = $point;
                    $result->point_position =$point_id;
                    $result->time = $time;
                    $result->save();
                    $point_id++;
                };
                unset($matches);
                preg_match_all('/Финиш:\s*\d{2}:\d{2}:\d{2}\s(\d{2}:\d{2}:\d{2})/', iconv('Windows-1251', 'UTF-8', $part), $matches);
                $result = new Result(['player_id' => $number, 'game' => 'day2']);
                $result->point_id = 'F';
                $result->point_position = $point_id;
                $result->time = $matches[1][0];
                $result->save();
                $i++;
            }
        }
    }

    public function actionNight(){
        die('Already done;');

        $dir = __DIR__ . '/../../';
        $file = file_get_contents($dir . 'night.html');
        $parts = explode('<hr>', $file);
        unset($parts[0]);
        foreach ($parts as $part) {
            preg_match_all('/№\s(\d+)/', iconv('Windows-1251', 'UTF-8', $part), $matches);
            $numbers = $matches[1];
            unset($matches);
            preg_match_all('/\(\s*(\d{2,3})\)\s\d{2}:\d{2}:\d{2}\s(\d{2}:\d{2}:\d{2})/', $part, $matches);
            $i = 0;
            foreach ($numbers as $number) {
                $point_id = 0;
                foreach (array_combine($matches[1], $matches[2]) as $point => $time) {
                    $result = new Result(['player_id' => $number, 'game' => 'night']);
                    $result->point_id = $point;
                    $result->point_position =$point_id;
                    $result->time = $time;
                    $result->save();
                    $point_id++;
                };
                unset($matches);
                preg_match_all('/Финиш:\s*\d{2}:\d{2}:\d{2}\s(\d{2}:\d{2}:\d{2})/', iconv('Windows-1251', 'UTF-8', $part), $matches);
                $result = new Result(['player_id' => $number, 'game' => 'night']);
                $result->point_id = 'F';
                $result->point_position = $point_id;
                $result->time = $matches[1][0];
                $result->save();
                $i++;
            }
        }
    }

    public function actionData(){
        $category = new Category();
        $categories = [];
        foreach ($category->find()->raw() as $category) {
            $categories[$category['id']] = $category['title'];
        }
        $categoriesJs = 'var Categories = ' . json_encode($categories, JSON_UNESCAPED_UNICODE) . ";\n";

        $player = new Player();
        $playersByCategory = [];
        $allPlayers = [];
        $playersData = $player
            ->find()
            ->select(['number', 'name', 'title', 'category_id'])
            ->join([Category::tableName() => Category::tableName() . '.id = ' . Player::tableName() . '.category_id'])
            ->raw();
        foreach ($playersData as $datum) {
            $allPlayers[$datum['number']] = $datum['name'];
            $playersByCategory[$datum['category_id']][$datum['number']] = $datum['name'];
        }

        $select2 = [];
        foreach ($playersData as $player) {
            if (!array_key_exists($player['title'], $select2))
                $select2[$player['title']][$player['number']] = $player['name'];
            else $select2[$player['title']][$player['number']] = $player['name'];
        }
        $data = [];
        foreach ($select2 as $category => $players) {
            $children = [];
            foreach ($players as $number => $player) {
                $children[] = ['id' => $number, 'text' => $player];
            }
            $data[] = ['text' => $category, 'children' => $children];
        }
        $playersJs = 'var PlayersByCategory = ' . json_encode($playersByCategory, JSON_UNESCAPED_UNICODE) . ";\n" .
        'var Players = ' . json_encode($allPlayers, JSON_UNESCAPED_UNICODE) . "\n" .
        'var PlayersSelect2 = ' . json_encode($data, JSON_UNESCAPED_UNICODE) . ";";

        file_put_contents(__DIR__ . '/../../js/data.js', $categoriesJs . $playersJs);
    }
}