$(document).ready(function () {
    var select2 = $('#player').select2({
        data:PlayersSelect2,
        placeholder: "Выберите участника"
    });
    select2.on('change', function (i, el) {
        Cookies.set('player', $(this).val());
    });
    $('.cssload-fond').detach();
    select2.parent().show();
});