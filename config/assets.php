<?php return array_merge_recursive(
    // default assets
    [
        'main' => [
            'css' => [__DIR__ . '/../css/pure.css'],
            'js' =>[
                __DIR__ . '/../vendor/components/jquery/jquery.min.js',
                __DIR__ . '/../vendor/bower/js-cookie/src/js.cookie.js',
            ]
        ]
    ],

    [
        'main' => [
            'css' => [__DIR__ . '/../vendor/select2/select2/dist/css/select2.min.css'],
            'js' => [__DIR__ . '/../vendor/select2/select2/dist/js/select2.min.js']
        ],
    ],
    // users
    [
        'main' => [
            'css' => [
                __DIR__ . '/../css/select2.override.css',
                __DIR__ . '/../css/style.css'
            ],
            'js' => [
                __DIR__ . '/../js/data.js',
                __DIR__ . '/../js/main.js'
            ]
        ]
    ]
);